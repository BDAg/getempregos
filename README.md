# GetEmpregos

Web Bot que faz a captura e exibição de todas as vagas de emprego na área de Tecnologia da informação no Estado de São Paulo.


## Sobre o GetEmpregos
O Programa GetEmpregos foi desenvolvido com o intuito de  buscar e capturar vagas de emprego disponiveis nos sites que ele foi programado para acessar.
O código faz a captura de todas as vagas fornecidas pelos sites por meio de web Scraping, e transforma esses dados em um arquivo CSV.
## Assista nosso Pitch:
clique aqui -> https://youtu.be/6gBiP_r1_UM
### Quer saber quem são os criadores desse projeto mágnifico?

**Entre em contato conosco:**

-  Luan Angelo Rocha = email: Luanrochaangelo@gmail.com 
-  Matheus Henrique Araújo = email: matheusha@live.com
-  Victor Rodrigues Smaniotto = email: victorsmaniotto@hotmail.com
- Jonathan Jacinto de Jesus = email : Jhonnyjacinto09@icloud.com

_Agradecemos pelo acesso..._
