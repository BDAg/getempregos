import requests
from bs4 import BeautifulSoup

url = "https://www.vagas.com.br/vagas-de-programador"
data_dir = "bdvagas"
response = requests.get(url)
response.text
soup = BeautifulSoup(response.text)
lista = []
for vagas in soup.find_all("div", attrs={"class": "informacoes-header"}):
    titulo = (vagas.h2.a['title'])
    link = (vagas.h2.a['href'])
    lista.append([titulo, link])
